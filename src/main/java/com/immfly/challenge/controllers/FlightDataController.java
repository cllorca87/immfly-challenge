package com.immfly.challenge.controllers;

import com.immfly.challenge.models.FlightData;
import com.immfly.challenge.services.interfaces.FlightDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(("/v1/flight-information"))
public class FlightDataController {

    private final FlightDataService flightDataService;

    @Autowired
    public FlightDataController(FlightDataService flightDataService) {
        this.flightDataService = flightDataService;
    }

    /**
     * Returns a list of the flights filtered by tail-number and (optional)flight-number
     * @param tailNumber
     * @param flightNumber
     * @return
     */
    @GetMapping(value = {"/{tail-number}", "/{tail-number}/{flight-number}"})
    public List<FlightData> getFlightDataByTailNumberAndFlightNumber(@PathVariable(value = "tail-number") String tailNumber,
                                                                     @PathVariable(value = "flight-number", required = false) String flightNumber) {

        if (flightNumber != null) {
            return flightDataService.getFlightDataByTailNumberAndFlightNumber(tailNumber, flightNumber);
        } else {
            return flightDataService.getFlightDataByTailNumber(tailNumber);
        }
    }

    /**
     * Creates a specified flightData in order to easily test the api
     * @return
     */
    @PostMapping("/dummy")
    public FlightData postFlightDataDummy(){
        return flightDataService.postFlightDataDummy();
    }

    /**
     * Inserts into the redis database the given flightData
     * @param flightData
     * @return
     */
    @PostMapping("")
    public FlightData postFlightData(@RequestBody FlightData flightData){
        return flightDataService.postFlightData(flightData);
    }
}
