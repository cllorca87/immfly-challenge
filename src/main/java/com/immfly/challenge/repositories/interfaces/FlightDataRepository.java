package com.immfly.challenge.repositories.interfaces;

import com.immfly.challenge.models.FlightData;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FlightDataRepository extends CrudRepository<FlightData, String> {

    @Override
    List<FlightData> findAll();
}
