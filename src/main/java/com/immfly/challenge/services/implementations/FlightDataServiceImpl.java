package com.immfly.challenge.services.implementations;

import com.immfly.challenge.models.Airport;
import com.immfly.challenge.models.FlightData;
import com.immfly.challenge.repositories.interfaces.FlightDataRepository;
import com.immfly.challenge.services.interfaces.FlightDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class FlightDataServiceImpl implements FlightDataService {

    private FlightDataRepository flightDataRepository;

    @Autowired
    public FlightDataServiceImpl(FlightDataRepository flightDataRepository) {
        this.flightDataRepository = flightDataRepository;
    }

    @Override
    public List<FlightData> getFlightDataByTailNumberAndFlightNumber(String tailNumber, String flightNumber) {
        return flightDataRepository.findAll().stream()
                .filter(flightData -> flightData.getTailnumber().equals(tailNumber))
                .filter(flightData -> flightData.getFlightnumber().equals(flightNumber))
                .collect(Collectors.toList());
    }

    @Override
    public List<FlightData> getFlightDataByTailNumber(String tailNumber) {
        return flightDataRepository.findAll().stream()
                .filter(flightData -> flightData.getTailnumber().equals(tailNumber))
                .collect(Collectors.toList());
    }

    @Override
    public FlightData postFlightDataDummy() {
        FlightData flightData = new FlightData();
        flightData.setIdent("IBB653");
        flightData.setFaFlightID("IBB653-1581399936-airline-0136");
        flightData.setAirline("IBB");
        flightData.setAirline_iata("NT");
        flightData.setFlightnumber("653");
        flightData.setTailnumber("EC-MYT");
        flightData.setType("Form_Airline");
        flightData.setCodeshares("IBE123");
        flightData.setBlocked(false);
        flightData.setDiverted(false);
        flightData.setCancelled(false);

        Airport origin = new Airport();
        origin.setCode("GCXO");
        origin.setCity("Tenerife");
        origin.setAlternate_ident("TFN");
        origin.setAirport_name("Tenerife North (Los Rodeos)");

        Airport destination = new Airport();
        destination.setCode("GCGM");
        destination.setCity("La Gomera");
        destination.setAlternate_ident("GMZ");
        destination.setAirport_name("La Gomera");

        flightData.setOrigin(origin);
        flightData.setDestination(destination);
        return flightDataRepository.save(flightData);
    }

    @Override
    public FlightData postFlightData(FlightData flightData) {
        return flightDataRepository.save(flightData);
    }
}
