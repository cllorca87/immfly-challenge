package com.immfly.challenge.services.interfaces;

import com.immfly.challenge.models.FlightData;

import java.util.List;

public interface FlightDataService {

    /**
     * Returns all flights filtered by tailNumber and flightNumber
     * @param tailNumber
     * @param flightNumber
     * @return
     */
    List<FlightData> getFlightDataByTailNumberAndFlightNumber(String tailNumber, String flightNumber);

    /**
     * Returns all flights filtered by tailNumber
     * @param tailNumber
     * @return
     */
    List<FlightData> getFlightDataByTailNumber(String tailNumber);

    /**
     * Creates a dummy FlightData in order to test easily the api
     * @return
     */
    FlightData postFlightDataDummy();

    /**
     * Inserts into the redis database the given flightData
     * @param flightData
     * @return
     */
    FlightData postFlightData(FlightData flightData);
}
