package com.immfly.challenge.services;

import com.immfly.challenge.models.FlightData;
import com.immfly.challenge.repositories.interfaces.FlightDataRepository;
import com.immfly.challenge.services.implementations.FlightDataServiceImpl;
import com.immfly.challenge.services.interfaces.FlightDataService;
import com.immfly.challenge.utils.TestData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FlightDataServiceImplTest {


    private FlightDataService flightDataServiceSut;

    @Mock
    private FlightDataRepository flightDataRepositoryMock;

    private TestData testData;

    @BeforeAll
    public void setUp(){
        MockitoAnnotations.openMocks(this);
        testData = new TestData();
        flightDataServiceSut = new FlightDataServiceImpl(flightDataRepositoryMock);
    }

    @Test
    public void getFlightDataByTailNumberAndFlightNumberSuccess(){

        List<FlightData> flightDataList = testData.flightDataListTestData();
        Mockito.when(flightDataRepositoryMock.findAll()).thenReturn(flightDataList);

        List<FlightData> result = flightDataServiceSut.getFlightDataByTailNumberAndFlightNumber("EC-MYT", "653");

        assertEquals(flightDataList, result);
    }

    @Test
    public void getFlightDataByTailNumberAndFlightNumberEmptySuccess(){

        Mockito.when(flightDataRepositoryMock.findAll()).thenReturn(Collections.emptyList());

        List<FlightData> result = flightDataServiceSut.getFlightDataByTailNumberAndFlightNumber("EC-MYT", "653");

        assertTrue(result.isEmpty());
    }

    @Test
    public void getFlightDataByTailNumberSuccess(){

        List<FlightData> flightDataList = testData.flightDataListTestData();
        Mockito.when(flightDataRepositoryMock.findAll()).thenReturn(flightDataList);

        List<FlightData> result = flightDataServiceSut.getFlightDataByTailNumber("EC-MYT");

        assertEquals(flightDataList, result);
    }

    @Test
    public void getFlightDataByTailNumberEmptySuccess(){

        Mockito.when(flightDataRepositoryMock.findAll()).thenReturn(Collections.emptyList());

        List<FlightData> result = flightDataServiceSut.getFlightDataByTailNumber("EC-MYT");

        assertTrue(result.isEmpty());
    }

    @Test
    public void postFlightDataDummySuccess(){
        FlightData flightData = testData.flightDataTestData();
        Mockito.when(flightDataRepositoryMock.save(Mockito.any(FlightData.class))).thenReturn(flightData);

        FlightData result = flightDataServiceSut.postFlightDataDummy();

        assertEquals(flightData, result);
    }

    @Test
    public void postFlightDataSuccess(){

        FlightData flightData = testData.flightDataTestData();
        Mockito.when(flightDataRepositoryMock.save(Mockito.any(FlightData.class))).thenReturn(flightData);

        FlightData result = flightDataServiceSut.postFlightData(flightData);

        assertEquals(flightData, result);
    }
}
