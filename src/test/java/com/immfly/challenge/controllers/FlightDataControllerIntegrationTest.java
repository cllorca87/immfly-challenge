package com.immfly.challenge.controllers;

import com.immfly.challenge.ChallengeApplication;
import com.immfly.challenge.repositories.interfaces.EmbeddedRedis;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {ChallengeApplication.class})
@Import(EmbeddedRedis.class)
@AutoConfigureMockMvc
public class FlightDataControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithMockUser(username = "admin", password = "admin", roles = "ADMIN")
    public void testAdminUserCanUseAPI() throws Exception {
        mockMvc.perform(get("/v1/flight-information/tail-number")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testNotSpecifyUserUnauthorizedRequest401() throws Exception {
        mockMvc.perform(get("/v1/flight-information/tail-number")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

}
