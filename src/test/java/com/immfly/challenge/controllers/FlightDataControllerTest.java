package com.immfly.challenge.controllers;

import com.immfly.challenge.models.FlightData;
import com.immfly.challenge.services.interfaces.FlightDataService;
import com.immfly.challenge.utils.TestData;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FlightDataControllerTest {

    @Mock
    private FlightDataService flightDataServiceMock;

    private FlightDataController flightDataControllerSut;

    private TestData testData;

    @BeforeAll
    public void setUp(){
        MockitoAnnotations.openMocks(this);
        testData = new TestData();
        flightDataControllerSut = new FlightDataController(flightDataServiceMock);
    }

    @Test
    public void getFlightDataByTailNumberAndFlightNumberSuccess(){

        List<FlightData> flightDataList = testData.flightDataListTestData();
        Mockito.when(flightDataServiceMock.getFlightDataByTailNumberAndFlightNumber("test", "test")).thenReturn(flightDataList);

        List<FlightData> result = flightDataControllerSut.getFlightDataByTailNumberAndFlightNumber("test", "test");

        assertEquals(flightDataList, result);
    }

    @Test
    public void getFlightDataByTailNumberAndFlightNumberEmptySuccess(){

        Mockito.when(flightDataServiceMock.getFlightDataByTailNumberAndFlightNumber("test", "test")).thenReturn(Collections.emptyList());

        List<FlightData> result = flightDataControllerSut.getFlightDataByTailNumberAndFlightNumber("test", "test");

        assertTrue(result.isEmpty());
    }

    @Test
    public void getFlightDataByTailNumberSuccess(){

        List<FlightData> flightDataList = testData.flightDataListTestData();
        Mockito.when(flightDataServiceMock.getFlightDataByTailNumber("test")).thenReturn(flightDataList);

        List<FlightData> result = flightDataControllerSut.getFlightDataByTailNumberAndFlightNumber("test", null);

        assertEquals(flightDataList, result);
    }

    @Test
    public void postFlightDataDummySuccess(){
        FlightData flightData = testData.flightDataTestData();
        Mockito.when(flightDataServiceMock.postFlightDataDummy()).thenReturn(flightData);

        FlightData result = flightDataControllerSut.postFlightDataDummy();

        assertEquals(flightData, result);
    }

    @Test
    public void postFlightDataSuccess(){

        FlightData flightData = testData.flightDataTestData();
        Mockito.when(flightDataServiceMock.postFlightData(flightData)).thenReturn(flightData);

        FlightData result = flightDataControllerSut.postFlightData(flightData);

        assertEquals(flightData, result);
    }
}
