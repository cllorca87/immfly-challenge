package com.immfly.challenge.repositories.interfaces;

import com.immfly.challenge.models.FlightData;
import com.immfly.challenge.utils.TestData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class FlightDataRepositoryTest {

    @Autowired
    private FlightDataRepository flightDataRepository;

    @Test
    public void testRetrieveDataFromEmbeddedRedis() {
        TestData testData = new TestData();
        flightDataRepository.save(testData.flightDataTestData());

        List<FlightData> flights = flightDataRepository.findAll();

        assertEquals(1, flights.size());
    }

}
