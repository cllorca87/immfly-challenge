package com.immfly.challenge.utils;

import com.immfly.challenge.models.Airport;
import com.immfly.challenge.models.FlightData;

import java.util.ArrayList;
import java.util.List;

public class TestData {

    public FlightData flightDataTestData(){
        FlightData flightData = new FlightData();
        flightData.setIdent("IBB653");
        flightData.setFaFlightID("IBB653-1581399936-airline-0136");
        flightData.setAirline("IBB");
        flightData.setAirline_iata("NT");
        flightData.setFlightnumber("653");
        flightData.setTailnumber("EC-MYT");
        flightData.setType("Form_Airline");
        flightData.setCodeshares("IBE123");
        flightData.setBlocked(false);
        flightData.setDiverted(false);
        flightData.setCancelled(false);

        Airport origin = new Airport();
        origin.setCode("GCXO");
        origin.setCity("Tenerife");
        origin.setAlternate_ident("TFN");
        origin.setAirport_name("Tenerife North (Los Rodeos)");

        Airport destination = new Airport();
        destination.setCode("GCGM");
        destination.setCity("La Gomera");
        destination.setAlternate_ident("GMZ");
        destination.setAirport_name("La Gomera");

        flightData.setOrigin(origin);
        flightData.setDestination(destination);

        return flightData;
    }

    public List<FlightData> flightDataListTestData(){
        List<FlightData> flightDataList = new ArrayList<>();
        flightDataList.add(flightDataTestData());
        return flightDataList;
    }
}
