# Getting Started

### Build and Run project

To start complete project, in the root project directory run the next command:

    mvn clean install
    docker-compose up


### Develop builds

You can use independent docker image for redis:

    cd docker/redis/
    docker image build -t challenge-redis .
    docker run --rm -p 6379:6379 --name challenge-redis -d challenge-redis

You're ready for start spring-boot application:
    
     mvn spring-boot:run -Dspring-boot.run.profiles=dev

Or compiling and starting:

    mvn clean install
    java -jar -Dspring.profiles.active=dev target/challenge-0.0.1-SNAPSHOT.jar 
    
### Additional Notes
This challenge project has been developed under the given documentation with its specifications, 
I would strongly recommend specifying a naming convention in order to make even cleaner code.
